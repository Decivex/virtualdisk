pub mod plain;
pub mod crypto;
pub mod memory;
pub mod error;

#[cfg(test)]
mod testutils;

use bincode::Options;
use error::DiskError;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub (crate) struct VersionHeader {
    disk_type: u32,
    version: u32
}

impl VersionHeader {
    fn size() -> u64 {
        bincode_options().serialized_size(&VersionHeader { disk_type: 0, version: 0  }).unwrap()
    }

    fn check(expected: &Self, actual: &Self) -> error::Result<()> {
        if expected.disk_type != actual.disk_type {
            return Err(DiskError::WrongType(expected.disk_type, actual.disk_type));
        }

        if expected.version != actual.version {
            return Err(DiskError::WrongVersion(expected.version, actual.version));
        }

        Ok(())
    }
}

pub (crate) fn bincode_options() -> impl bincode::Options {
    bincode::config::DefaultOptions::new().with_big_endian().with_fixint_encoding()
}

pub trait VirtualDisk {
    /// Increase the size of the file by `size`.
    /// 
    /// This function should return the new inner size of the file, without counting the header of the virtual disk.
    fn grow(&mut self, size: usize) -> error::Result<usize>;

    /// Read the bytes located at offset into the buffer.
    fn read_at(&mut self, buffer: &mut [u8], offset: u64) -> error::Result<()>;

    fn write_at(&mut self, buffer: &[u8], offset: u64) -> error::Result<()>;

    fn size(&mut self) -> error::Result<usize>;
}