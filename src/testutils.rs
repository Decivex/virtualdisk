use std::path::{PathBuf, Path};

use tempfile::TempDir;

pub struct TmpPath {
    _dir: TempDir,
    path: PathBuf
}

impl TmpPath {
    pub fn from(filename: &'static str) -> Self {
        let dir: TempDir = TempDir::new().unwrap();
        let path = dir.as_ref().join(filename);
        Self { _dir: dir, path }
    }
}

impl AsRef<Path> for TmpPath {
    fn as_ref(&self) -> &Path {
        &self.path
    }
}