use std::fs::File;
use std::io::{SeekFrom, Seek, Write, Read};
use std::path::Path;

use bincode::Options;
use rand::{rngs::OsRng, Rng};
use serde::{Serialize, Deserialize};
use sha2::{Sha256, Digest};
use aes::cipher::{KeyIvInit, StreamCipher, StreamCipherSeek};

use crate::{VersionHeader, bincode_options, VirtualDisk};
use crate::error::{Result, DiskError};

type Aes128Ctr64LE = ctr::Ctr64LE<aes::Aes128>;

const VERSION_HEADER: VersionHeader = VersionHeader {
    disk_type: 0xAE800C40,
    version: 0x00_00_00_01,
};

const KEY_SIZE: usize = 16;
const IV_SIZE: usize = KEY_SIZE;

pub struct CryptoDisk {
    inner: File,
    cipher: Aes128Ctr64LE
}

#[derive(PartialEq, Debug)]
pub struct Key {
    bytes: [u8; KEY_SIZE]
}

#[derive(Serialize, Deserialize, Clone)]
pub struct CryptoHeader {
    iv: [u8; IV_SIZE],
    encrypted_key: [u8; KEY_SIZE],
}

impl Key {
    pub fn new(bytes: &[u8; KEY_SIZE]) -> Self {
        Self { bytes: bytes.clone() }
    }

    pub fn from_password(password: &str) -> Self {
        let mut hasher = Sha256::new();
        hasher.update(password.as_bytes());
        let result = hasher.finalize();
        Self {
            bytes: result[0..16].try_into().unwrap()
        }
    }
}

impl CryptoDisk {
    pub fn create<P: AsRef<Path>>(path: P, key: &Key) -> Result<Self> {
        let file = File::options()
                .read(true)
                .write(true)
                .create_new(true)
                .open(path)?;

        let mut iv = [0u8; 16];
        OsRng.fill(&mut iv);
        
        if iv.iter().all(|&b| b==0u8) {
            Err(DiskError::Custom("Error initializing IV".into()))?;
        }

        let mut cipher = Aes128Ctr64LE::new(&key.bytes.into(), iv.as_slice().into());
        let mut encrypted_key = key.bytes.clone();

        cipher.apply_keystream(&mut encrypted_key);
        assert_ne!(key.bytes, encrypted_key);

        let crypto_header = CryptoHeader { iv, encrypted_key };

        bincode_options().serialize_into(&file, &VERSION_HEADER)?;
        bincode_options().serialize_into(&file, &crypto_header)?;

        Ok(Self { inner: file, cipher })
    }

    pub fn open<P: AsRef<Path>>(path: P, key: &Key) -> Result<Self> {
        let file = File::options()
                .read(true)
                .write(true)
                .open(path)?;
        let version_header: VersionHeader = bincode_options().deserialize_from(&file)?;

        VersionHeader::check(&VERSION_HEADER, &version_header)?;

        let crypto_header: CryptoHeader = bincode_options().deserialize_from(&file)?;

        let iv = crypto_header.iv.clone();
        let cipher = Aes128Ctr64LE::new(&key.bytes.into(), iv.as_slice().into());

        Ok(Self { inner: file, cipher })
    }

    pub fn read_header(&mut self) -> Result<CryptoHeader> {
        self.inner.seek(SeekFrom::Start(VersionHeader::size()))?;
        Ok(bincode_options().deserialize_from(&self.inner)?)
    }

    pub fn verify_key(&mut self, key: &Key) -> Result<bool> {
        let mut key_buffer = [0u8; KEY_SIZE];
        self.cipher.seek(0usize);
        self.cipher.apply_keystream_b2b(key.bytes.as_slice(), &mut key_buffer)?;
        
        let crypto_header = self.read_header()?;
        
        Ok(crypto_header.encrypted_key == key_buffer)
    }

    fn inner_len(&mut self) -> Result<u64> {
        Ok(self.inner.seek(SeekFrom::End(0))?)
    }

    fn seek(&mut self, offset: u64) -> Result<()> {
        let offset = offset + VersionHeader::size() + IV_SIZE as u64 + KEY_SIZE as u64;
        self.inner.seek(SeekFrom::Start(offset))?;
        self.cipher.seek(offset - KEY_SIZE as u64);
        Ok(())
    }
}

impl VirtualDisk for CryptoDisk {
    fn grow(&mut self, size: usize) -> crate::error::Result<usize> {
        let mut i = size;
        let random_buffer = &mut [0u8; 16];
        self.inner.seek(SeekFrom::End(0))?;
        while i > 0 {
            OsRng.fill(random_buffer);
            let len = i.min(random_buffer.len());
            self.inner.write_all(&random_buffer[0..len])?;
            i -= len;
        }

        Ok(self.size()?)
    }

    fn read_at(&mut self, buffer: &mut [u8], offset: u64) -> crate::error::Result<()> {
        if offset as usize + buffer.len() > self.size()? {
            Err(DiskError::NotEnoughSpace)?;
        }

        self.seek(offset)?;
        self.inner.read_exact(buffer)?;
        self.cipher.apply_keystream(buffer);

        Ok(())
    }

    fn write_at(&mut self, buffer: &[u8], offset: u64) -> crate::error::Result<()> {
        if offset as usize + buffer.len() > self.size()? {
            Err(DiskError::NotEnoughSpace)?;
        }

        self.seek(offset)?;
        let mut write_buffer = vec![0u8; buffer.len()];
        self.cipher.apply_keystream_b2b(buffer, &mut write_buffer)?;
        self.inner.write_all(&write_buffer)?;

        Ok(())
    }

    fn size(&mut self) -> crate::error::Result<usize> {
        Ok(self.inner_len()? as usize - VersionHeader::size() as usize - IV_SIZE as usize - KEY_SIZE as usize)
    }
}

#[cfg(test)]
mod tests {
    use hex_literal::hex;

    use crate::VirtualDisk;
    use crate::testutils::TmpPath;

    use super::{Key, CryptoDisk};

    #[test]
    fn create_key() {
        let expected = Key::new(&hex!(
            "f52fbd32b2b3b86ff88ef6c490628285"
        ));
        let actual = Key::from_password("hunter2");
        assert_eq!(expected, actual);
    }

    #[test]
    fn create_and_open_disk() {
        let path = TmpPath::from("crypto_create");
        let key = Key::from_password("hunter2");

        CryptoDisk::create(&path, &key).unwrap();
        
        CryptoDisk::open(&path, &key).unwrap();
    }

    #[test]
    fn grow_disk() {
        let path = TmpPath::from("crypto_grow");
        let key = Key::from_password("hunter2");

        let mut disk = CryptoDisk::create(&path, &key).unwrap();
        assert_eq!(0usize, disk.size().unwrap());

        let new_size = disk.grow(64).unwrap();
        assert_eq!(64usize, new_size);
        assert_eq!(64usize, disk.size().unwrap());
    }

    #[test]
    fn write_and_read_disk() {
        let path = TmpPath::from("crypto_rw");
        let key = Key::from_password("hunter2");

        let mut disk = CryptoDisk::create(&path, &key).unwrap();
        let offset = 8;
        let buffer = b"The quick brown fox jumps over the lazy dog.";
        disk.grow(offset + buffer.len()).unwrap();
        disk.write_at(buffer, offset as u64).unwrap();
        drop(disk);

        let mut disk = CryptoDisk::open(&path, &key).unwrap();
        let mut output_buffer = vec![0u8; buffer.len()];
        disk.read_at(output_buffer.as_mut_slice(), offset as u64).unwrap();

        assert_eq!(buffer, output_buffer.as_slice());
    }

    #[test]
    fn verify_key() {
        let path = TmpPath::from("crypto_verify");
        let key = Key::from_password("hunter2");
        let mut disk = CryptoDisk::create(&path, &key).unwrap();
        let valid = disk.verify_key(&Key::from_password("hunter2")).unwrap();
        assert!(valid);
    }
}