use std::error::Error;
use std::result;
use std::io;

use aes::cipher::StreamCipherError;

#[derive(Debug)]
pub enum DiskError {
    IO(io::Error),
    NotEnoughSpace,
    WrongType(u32, u32),
    WrongVersion(u32, u32),
    Custom(String),
    CipherError(StreamCipherError),
    Other(Box<dyn Error>)
}

pub type Result<T> = result::Result<T, DiskError>;

impl From<io::Error> for DiskError {
    fn from(err: io::Error) -> Self {
        DiskError::IO(err)
    }
}

impl From<bincode::Error> for DiskError {
    fn from(err: bincode::Error) -> Self {
        DiskError::Other(Box::new(err))
    }
}

impl From<StreamCipherError> for DiskError {
    fn from(err: StreamCipherError) -> Self {
        DiskError::CipherError(err)
    }
}