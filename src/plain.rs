use std::io::{self, Write, Seek, Read};
use std::fs::File;
use std::path::Path;

use bincode::Options;

use crate::{VersionHeader, bincode_options};
use crate::VirtualDisk;
use crate::error::{Result, DiskError};

const TYPE_ID: u32 = 1;
const VERSION: u32 = 0x00_00_00_01;

const WRONG_TYPE_MSG: &str = "wrong disk type";
pub struct PlainDisk {
    inner: File
}

impl PlainDisk {
    pub fn create<P: AsRef<Path>>(path: P) -> bincode::Result<Self> {
        let file = File::options()
            .read(true)
            .write(true)
            .create_new(true)
            .open(path)?;

        let binopt = bincode::config::DefaultOptions::new().with_big_endian().with_fixint_encoding();
        let header = VersionHeader {
            disk_type: TYPE_ID,
            version: VERSION,
        };

        binopt.serialize_into(&file, &header)?;

        Ok(Self { inner: file })
    }

    pub fn open<P: AsRef<Path>>(path: P) -> bincode::Result<Self> {
        let file = File::options()
            .read(true)
            .write(true)
            .open(path)?;
        
        let header: VersionHeader = bincode_options().deserialize_from(&file)?;

        if header.disk_type != TYPE_ID {
            Err(io::Error::new(io::ErrorKind::Other, WRONG_TYPE_MSG))?;
        }

        let version = header.version;
        if version != VERSION {
            Err(io::Error::new(io::ErrorKind::Other, format!(
                "tried to load plain disk of version {version}, current version is {VERSION}, migrations are not supported"
            )))?;
        }

        Ok(Self { inner: file })
    }

    fn seek(&mut self, offset: u64) -> io::Result<()> {
        self.inner.seek(io::SeekFrom::Start(VersionHeader::size() + offset))?;
        Ok(())
    }
}

impl VirtualDisk for PlainDisk {
    fn grow(&mut self, size: usize) -> Result<usize> {
        let inner_len = self.inner.seek(io::SeekFrom::End(0))?;
        self.inner.set_len(inner_len + size as u64)?;
        self.size()
    }

    fn read_at(&mut self, buffer: &mut [u8], offset: u64) -> Result<()> {
        if offset as usize + buffer.len() > self.size()? {
            Err(DiskError::NotEnoughSpace)?;
        }

        self.seek(offset)?;
        self.inner.read_exact(buffer)?;
        Ok(())
    }

    fn write_at(&mut self, buffer: &[u8], offset: u64) -> Result<()> {
        if offset as usize + buffer.len() > self.size()? {
            Err(DiskError::NotEnoughSpace)?;
        }

        self.seek(offset)?;
        self.inner.write_all(buffer)?;

        Ok(())
    }

    fn size(&mut self) -> Result<usize> {
        let inner_size = self.inner.seek(io::SeekFrom::End(0))? as usize;
        Ok(inner_size - VersionHeader::size() as usize)
    }
}

#[cfg(test)]
mod tests {
    use std::io::Seek;
    use std::fs::{self, File};
    use std::path::PathBuf;

    use tempfile::TempDir;

    use crate::VirtualDisk;
    use crate::testutils::TmpPath;
    use super::*;

    #[test]
    fn test_create_empty_disk() {
        let dir = TempDir::new().unwrap();
        let mut path = PathBuf::from(dir.path());
        path.push("empty");

        PlainDisk::create(&path).unwrap();

        assert!(path.exists());
        let buffer = fs::read(path).unwrap();
        assert_eq!(buffer[0..4], TYPE_ID.to_be_bytes());
        assert_eq!(buffer[4..8], VERSION.to_be_bytes());
    }

    #[test]
    fn test_grow_disk() {
        let dir = TempDir::new().unwrap();
        let mut path = PathBuf::from(dir.path());
        path.push("grow");

        let mut disk = PlainDisk::create(&path).unwrap();
        disk.grow(16).unwrap();

        drop(disk);

        let mut file = File::open(path).unwrap();
        let size = file.seek(std::io::SeekFrom::End(0)).unwrap();

        assert_eq!(VersionHeader::size() + 16, size);
    }

    #[test]
    fn test_write_space_error() {
        let dir = TempDir::new().unwrap();
        let mut path = PathBuf::from(dir.path());
        path.push("spaceerror");

        let mut disk = PlainDisk::create(&path).unwrap();

        let buffer = b"Hello world!";

        disk.grow(16).unwrap();

        let result = disk.write_at(buffer, 12);

        assert!(result.is_err());
    }

    #[test]
    fn test_write() {
        let testbuffer = b"Hello world!";

        let dir = TempDir::new().unwrap();
        let mut path = PathBuf::from(dir.path());
        path.push("write");

        let mut disk = PlainDisk::create(&path).unwrap();
        disk.grow(16).unwrap();
        disk.write_at(testbuffer, 2).unwrap();

        drop(disk);

        let buffer = fs::read(&path).unwrap();

        assert_eq!(buffer[0..4], TYPE_ID.to_be_bytes());
        assert_eq!(buffer[4..8], VERSION.to_be_bytes());
        let offset = VersionHeader::size() as usize + 2;
        assert_eq!(&buffer[offset..offset+testbuffer.len()], testbuffer);
    }

    #[test]
    fn test_read() {
        let dir = TempDir::new().unwrap();
        let mut path = PathBuf::from(dir.path());
        path.push("read");

        let file_contents = b"\x00\x00\x00\x01\x00\x00\x00\x01...Hello world!";
        fs::write(&path, file_contents).unwrap();

        let mut disk = PlainDisk::open(&path).unwrap();
        let buffer = &mut [0; 12];
        disk.read_at(buffer, 3).unwrap();
        assert_eq!(buffer, b"Hello world!")
    }

    #[test]
    fn prevent_overwrite() {
        let path = TmpPath::from("do_not_overwrite");
        PlainDisk::create(&path).unwrap();
        let result = PlainDisk::create(&path);
        assert!(result.is_err());
    }
}