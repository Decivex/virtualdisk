use crate::{VirtualDisk, error::{Result, DiskError}};

pub struct MemoryDisk {
    memory: Vec<u8>
}

impl MemoryDisk {
    pub fn new() -> Self {
        Self {
            memory: vec![]
        }
    }
}

impl VirtualDisk for MemoryDisk {
    fn grow(&mut self, size: usize) -> Result<usize> {
        for _ in 0..size {
            self.memory.push(0);
        }
        Ok(self.memory.len())
    }

    fn read_at(&mut self, buffer: &mut [u8], offset: u64) -> Result<()> {
        if offset as usize + buffer.len() > self.size()? {
            Err(DiskError::NotEnoughSpace)?;
        }

        let offset = offset as usize;
        buffer.copy_from_slice(&self.memory[offset..offset+buffer.len()]);

        Ok(())
    }

    fn write_at(&mut self, buffer: &[u8], offset: u64) -> Result<()> {
        if offset as usize + buffer.len() > self.size()? {
            Err(DiskError::NotEnoughSpace)?;
        }

        let offset = offset as usize;
        self.memory[offset..offset+buffer.len()].copy_from_slice(buffer);

        Ok(())
    }

    fn size(&mut self) -> Result<usize> {
        Ok(self.memory.len())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_write_space_error() {
        let mut disk = MemoryDisk::new();

        let buffer = b"Hello world!";

        disk.grow(16).unwrap();

        let result = disk.write_at(buffer, 12);

        assert!(result.is_err());
    }

    #[test]
    fn test_write() {
        let testbuffer = b"Hello world!";

        let mut disk = MemoryDisk::new();
        disk.grow(16).unwrap();
        disk.write_at(testbuffer, 2).unwrap();

        assert_eq!(testbuffer, &disk.memory[2..2+testbuffer.len()]);

    }

    #[test]
    fn test_read() {
        const disk_memory: &[u8] = b"...Hello world!";
        const test_string: &[u8] = b"Hello world!";

        let mut disk =  MemoryDisk {
            memory: disk_memory.to_vec()
        };

        let buffer = &mut [0; test_string.len()];
        disk.read_at(buffer, 3).unwrap();
        assert_eq!(buffer, test_string);
    }
}